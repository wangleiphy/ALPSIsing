'''
python tau.py -f ../data/test 
'''

import pyalps
from report import report

import argparse
parser = argparse.ArgumentParser(description='')
parser.add_argument("-fileheaders", nargs='+', default="params", help="fileheaders")
args = parser.parse_args()

resultFiles = []
for fileheader in args.fileheaders:
    resultFiles += pyalps.getResultFiles(prefix=fileheader)

resultFiles = list(set(resultFiles))

for filename in resultFiles:

    obslist = ['M2', 'M4', 'E', 'E2','Cv','Binder']

    print filename 
    report(filename, obslist)
    print "#######################################################################"
