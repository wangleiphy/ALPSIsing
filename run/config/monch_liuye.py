import subprocess 
from numpy import arange, array 
import re
import time

tup = 1.0 
tdnList = [0.0]

RESET_MEASUREMENTS = 0
MEASURE_M4 = 1
MEASURE_G = 1
##################do not change 
Add = 0.5
Remove = 0.5
itime_max = 1<<31
NSTORAGE = 128 
NBLOCKlist = [256, 512, 1024, 2048]
##################do not change 

Maxorder = 16384 

#latticename = 'chain lattice'
#latticename = 'square lattice'
#latticename = 'honeycomb lattice'
#latticename = 'simple cubic lattice'
latticename = 'anisotropic square lattice'
###############################
nickname = 'finiteT-lct-aux tx ty'

Llist = array([4, 8, 12, 16])
Wlist = Llist 

#BETAlist = [16.0]
#BETAlist = [0.4, 0.6, 0.7, 0.8, 3, 4, 5, 6, 7, 8, 9]
#BETAlist = 1./array([0.08, 0.09])
#BETAlist = 1./arange(0.21, 0.3, 0.01)
#BETAlist = 1./arange(0.1, 1, 0.1)
#BETAlist = 1./arange(1., 10., 1.)
#BETAlist = 1./arange(0.14, 0.2, 0.01)
BETAlist = 1./arange(0.07, 0.15, 0.02)

Ulist = array([3.0])
#Ulist = array([0.4, 1.0, 2.0, 4.0])
#Ulist = arange(0.5, 4.0, 0.5)
#Gammalist = arange(-0.99, -0.9, 0.01)
Gammalist = -0.25*Ulist+0.05 

RECALC_PERIOD = 31
WRAP_REFRESH_PERIOD = 31

STEPS_PER_BLOCK = 1
THERMALIZATION = 10**5            # in unit of blocks
SWEEPS = 5*10**6                  # in unit of the the whole system 
MEASUREMENT_PERIOD = 107           # in unit of block

##############################
wtime = '24:00:00'
tmin = 60
tmax = 300
ncores = 100  # a multiply of ntasks_per_node 
prog = '../bin/main'

resfolder = '/mnt/lnec/liuye/AsymmHubbarddata/' + nickname  + '/'
h, m, s = [int(i) for i in wtime.split(':')]
Tlimit = max(3600*h + 60*m + s - int(tmax*4) , 0)
prog += ' -i '+ str(tmin) + ' -a ' + str(tmax) + ' -T ' + str(Tlimit) + ' -c '

def submitJob(bin,args,jobname,wtime,partition="dphys_compute", run=False,ncores=20, wait=None):

#SBATCH --ntasks=%g
    #prepare the job file 
    job='''#!/bin/bash -l
#
#SBATCH --exclusive
#SBATCH --nodes=%g
#SBATCH --time=%s
#SBATCH --partition=%s
#SBATCH --ntasks-per-node=20
#SBATCH --ntasks-per-socket=10
#SBATCH --cpus-per-task=1
#SBATCH --job-name=%s
#SBATCH --output=%s
#SBATCH --error=%s'''%(ncores/20, wtime,partition,jobname,jobname+'.log',jobname+'.log')

    if wait is not None:
        dependency ='''
#SBATCH --dependency=afterany:%d\n'''%(wait)
        job += dependency 


    job += '''
echo "The current job ID is $SLURM_JOB_ID"
echo "Running on $SLURM_JOB_NUM_NODES nodes:"
echo $SLURM_JOB_NODELIST
echo "Using $SLURM_NTASKS_PER_NODE tasks per node"
echo "A total of $SLURM_NTASKS tasks is used"\n'''

    job +='mpirun -rmk slurm '+ str(bin) + ' '
    for key, val in args.items():
        job += str(key) +' '+ str(val) + ' '
    job += '\n'

    #print job
    jobfile = open("jobfile", "w")
    jobfile.write("%s"%job)
    jobfile.close()

    #submit the job 
    if run:
        cmd = ['sbatch', 'jobfile']

        ret = subprocess.check_output(cmd)

        jobid = int(re.search(r'\d+', ret).group())
        print jobid , 'submitted'
        time.sleep(0.1)
        return jobid 

    else:

        cmd = ['cat','jobfile']
        subprocess.check_call(cmd)
        return None

