import subprocess 
from numpy import arange, array 
import time 

#latticename = 'chain lattice'
#latticename = 'square lattice'
latticename = 'honeycomb lattice'
#latticename = 'simple cubic lattice'
###############################
nickname = 'firsttry'

Llist = array([10, 20, 30, 40])
Wlist = Llist 
Tlist = arange(1.0, 2.0, 0.02)

THERMALIZATION = 500000        # in unit of sweep block
SWEEPS = 10000000               
##############################
tmin = 60
tmax = 300
ncores = 16 
wtime = '1:00:00'
bin = '../bin/main'

resfolder = '../data/' + nickname  + '/'
h, m, s = [int(i) for i in wtime.split(':')]
Tlimit = max(3600*h + 60*m + s - int(tmax*6) , 0)
prog = 'mpirun '+ bin  + ' -i '+ str(tmin) + ' -a ' + str(tmax) + ' -T ' +str(Tlimit) + ' -c '

def submitJob(bin,args,jobname,wtime,run=False,ncores=16, wait=None):

    #prepare the job file 
    job='''#!/bin/bash -l
#
#PBS -l nodes=%g:ppn=%g 
#PBS -N %s
#PBS -o %s
#PBS -e %s
'''%(ncores/16, ncores, jobname,jobname+'.log',jobname+'.log')

    if wait is not None:
        dependency ='''
#SBATCH --dependency=afterany:%d\n'''%(wait)
        job += dependency


    job += '''
ncpu=`cat $PBS_NODEFILE | wc -l`
cd $PBS_O_WORKDIR
echo Job started at `date`\n'''
    job +='mpirun -machinefile $PBS_NODEFILE -np $ncpu '+ str(bin) + ' '
    for key, val in args.items():
        job += str(key) +' '+ str(val) + ' '
    job += '''
echo Job finished at `date`\n'''

    #print job
    jobfile = open("jobfile", "w")
    jobfile.write("%s"%job)
    jobfile.close()

    #submit the job 
    if run:
        cmd = ['qsub', 'jobfile']
    else:
        cmd = ['cat','jobfile']

    subprocess.check_call(cmd)
    return None
