import subprocess 
from numpy import arange, array 

#latticename = 'chain lattice'
latticename = 'square lattice'
#latticename = 'honeycomb lattice'
#latticename = 'simple cubic lattice'
###############################
nickname = 'test'

Llist = array([10, 20, 30, 40])
Wlist = Llist 
Tlist = arange(1.8, 2.1, 0.02)

THERMALIZATION = 10**5            
SWEEPS = 5*10**6                  
##############################
tmin = 60
tmax = 300
ncores = 16 
wtime = '24:00'
bin = '../bin/main'

resfolder = '/cluster/work/scr1/lewang/ALPSISING/' + nickname  + '/'
h, m = [int(i) for i in wtime.split(':')]
Tlimit = max(3600*h + 60*m - int(tmax*2) , 0)

prog = 'mpirun '+ bin  + ' -i '+ str(tmin) + ' -a ' + str(tmax) + ' -T ' + str(Tlimit) + ' -c '

def submitJob(bin,args,jobname,wtime,run=False,ncores=None, wait=[]):

        if run:
            cmd = ['bsub']
        else:
            cmd = ['echo', 'bsub']

        if ncores != None:
            cmd += ['-n', str(ncores)]

        cmd += ['-W',str(wtime)] 
        cmd += ['-J', jobname]  # LSF jobname
        cmd += ['-oo', jobname+'.log'] #log file 
        
        if wait is not None:
            if len(wait) > 0:
                conds='ended('+wait[0]+')'
                for w in wait[1:]: conds += '&&ended('+w+')'
                cmd += ['-w',conds]

        #cmd += ['-R', '\"rusage[mem=4096]\"'] #memory usage 

        cmd += [bin] 

        for key, val in args.items():
            cmd += [key, val]

        subprocess.check_call(cmd)
        #time.sleep(2)
        return [jobname]
