#!/usr/bin/env python
from numpy import linspace 
from params import params 
import os.path 
import sys 

if __name__=='__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-run", action='store_true', help="Run or not")
    parser.add_argument("-waitfor", type=int, help="wait for this job for finish")
    input = parser.parse_args()
    #default parameters 

    jobdir='../jobs/'
    
    #this import might overwrite the above default parameters 
    #########################################################
    import socket, getpass
    machinename = socket.gethostname()
    username = getpass.getuser()
    print '\n', username, 'on', machinename, '\n'
    if 'brutus' in machinename:
        # from config.brutus import *
        if username == 'liuye':
            from config.brutus_liuye import *
        elif username == 'lewang':
            from config.brutus_lewang import *
    elif 'rosa' in machinename:
        from config.rosa import * 
    elif 'monch' in machinename:
        if username == 'liuye':
            from config.monch_liuye import *
        elif username == 'lewang':
            from config.monch_lewang import *
    elif 'node31' in machinename:
        from config.eta import * 
    else:
        print 'where am I ?', machinename 
        sys.exit(1)
    #########################################################

    cmd = ['mkdir', '-p', resfolder]
    subprocess.check_call(cmd)
    

    for L, W in zip(Llist, Wlist):
        jobid = input.waitfor 
        for T in Tlist: 

                           #generate the inputfile  
                           inputfile = params(latticename, L, W, T, 
                                              SWEEPS=SWEEPS, 
                                              THERMALIZATION=THERMALIZATION, 
                                              folder=resfolder 
                                              )

               
                           bin = prog + ' ' + inputfile 
               
                           args = {}
                           jobname = jobdir + os.path.basename(inputfile).replace('.in','')
               
                           jobid = submitJob(bin,args,jobname,wtime,ncores=ncores,run=input.run, wait=None)
