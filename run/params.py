import os.path 
import subprocess
import re , sys 
from math import cos , sin , pi 

def writeParameterFile(fname,parms):
    """ This function writes a text input file for simple ALPS applications like DMFT
    
        The arguments are:
        
          filename: the name of the parameter file to be written
          parms: the parameter dict
    """
    f = file(fname,'w')
    for key in parms:
      value = parms[key]
      if type(value) == str:
        f.write(str(key)+' = "' + value + '"\n')
      else:
        f.write(str(key)+' = ' + str(value) + '\n')
    f.close()
    return fname

'''
write parameters for main 
'''

def params(lattice, L, W, T, SWEEPS=1000000, THERMALIZATION=100000, folder='../data/'):
    
    key = lattice.replace(' ','') 
    key +=  'L' + str(L)\
           +'W' + str(W)\
           +'T'+str(T)\
           +'Therm'+str(THERMALIZATION)\
           +'Sweeps'+str(SWEEPS) 

    inputname = '../jobs/'+ key +'.in'
    outputname = folder + key +'.dat'
    
    parms ={'LATTICE_LIBRARY' : "../input/mylattices.xml"
            # above we should not change 

            ,'LATTICE'  : lattice
            ,'filename' : outputname

            ,'L'  : L 
            ,'W'  : W
            ,'T'  : T

            ,'THERMALIZATION' : THERMALIZATION
            ,'SWEEPS' : SWEEPS 

            }

    writeParameterFile(inputname, parms)

    return inputname 
