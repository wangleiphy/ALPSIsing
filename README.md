A demo project that shows
- ALPS libraries: lattice, mc, hdf5, pyalps  
- Build system
- Job submission
- Workflow & Provenance
