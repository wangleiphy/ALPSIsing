#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}  -I/cluster/home/phys/lewang/Libs/include/eigen3/")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DEIGEN_USE_MKL_ALL -I/cluster/home/phys/lewang/Libs/include/eigen3/")

if($ENV{USER} STREQUAL "lewang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DEIGEN_USE_MKL_ALL -I/cluster/home/phys/lewang/Libs/include/eigen3/")
elseif($ENV{USER} STREQUAL "liuye")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DEIGEN_USE_MKL_ALL -I/cluster/home02/phys/liuye/Eigen3/")
endif()
