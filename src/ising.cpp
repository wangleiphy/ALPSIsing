/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                 *
 * ALPS Project: Algorithms and Libraries for Physics Simulations                  *
 *                                                                                 *
 * ALPS Libraries                                                                  *
 *                                                                                 *
 * Copyright (C) 2010 - 2013 by Lukas Gamper <gamperl@gmail.com>                   *
 *                                                                                 *
 * This software is part of the ALPS libraries, published under the ALPS           *
 * Library License; you can use, redistribute it and/or modify it under            *
 * the terms of the license, either version 1 or (at your option) any later        *
 * version.                                                                        *
 *                                                                                 *
 * You should have received a copy of the ALPS Library License along with          *
 * the ALPS Libraries; see the file LICENSE.txt. If not, the license is also       *
 * available from http://alps.comp-phys.org/.                                      *
 *                                                                                 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        *
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT       *
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE       *
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,     *
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER     *
 * DEALINGS IN THE SOFTWARE.                                                       *
 *                                                                                 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "ising.hpp"

#include <alps/ngs/make_deprecated_parameters.hpp>
#include <boost/lambda/lambda.hpp>

ising_sim::ising_sim(parameters_type const & parms, std::size_t seed_offset)
    : alps::mcbase(parms, seed_offset)
    , lattice(make_deprecated_parameters(parms))
    , nsite(lattice.num_sites())
    , sweeps(0)
    , thermalization_sweeps(unsigned(parameters["THERMALIZATION"]))
    , total_sweeps(unsigned(parameters["SWEEPS"]))
    , beta(1. / double(parameters["T"]))
    , spins(nsite)
{
    for(unsigned i = 0; i < nsite; ++i)
        spins[i] = (random() < 0.5 ? 1 : -1);

    measurements
        << alps::accumulator::RealObservable("E")
        << alps::accumulator::RealObservable("E2")
        << alps::accumulator::RealObservable("M2")
        << alps::accumulator::RealObservable("M4")
    ;
}

void ising_sim::update() {
    for (unsigned i = 0; i < nsite; ++i) {

        unsigned si = randomint(nsite); 
        
        double de = 0.0; 
        for (unsigned j=0; j<lattice.num_neighbors(si); ++j){
            unsigned sj = lattice.neighbor(si, j); 
            de -= 2. * beta * spins[si] * spins[sj]; 
        }

        if ( de>0.0 || std::exp(de) > random() ){
            spins[si] = -spins[si];
            //std::cout << de <<  " flip "  << std::endl; 
        }else{
            //std::cout << de <<  " rejct "  << std::endl; 
        }
       
    }
}

void ising_sim::measure() {
    sweeps++;
    if (sweeps > thermalization_sweeps) {

        //std::ostream_iterator<int> out_it (std::cout,", ");
        //std::copy (spins.begin(), spins.end(), out_it);
        //std::cout<<std::endl; 

        double tmag = 0;
        double ten = 0;
        for (unsigned si = 0; si < nsite; ++si) {
            tmag += spins[si];
            for (unsigned j=0; j<lattice.num_neighbors(si); ++j){
                unsigned sj = lattice.neighbor(si, j); 
                ten += -spins[si] * spins[sj]; 
            }
        }
        tmag /= nsite;
        ten /= (2.*nsite);// 2 accounts for double counting 

        measurements["E"] << ten;
        measurements["E2"] << ten * ten;
        measurements["M2"] << tmag * tmag;
        measurements["M4"] << tmag * tmag * tmag * tmag;
    }
}

double ising_sim::fraction_completed() const {
    return (sweeps < thermalization_sweeps ? 0. : ( sweeps - thermalization_sweeps ) / double(total_sweeps));
}

void ising_sim::save(alps::hdf5::archive & ar) const {
    mcbase::save(ar);

    std::string context = ar.get_context();
    ar.set_context("/simulation/realizations/0/clones/0/checkpoint");
    ar["sweeps"] << sweeps;
    ar["spins"] << spins;
    ar.set_context(context);
}

void ising_sim::load(alps::hdf5::archive & ar) {
    mcbase::load(ar);

    std::string context = ar.get_context();
    ar.set_context("/simulation/realizations/0/clones/0/checkpoint");
    ar["sweeps"] >> sweeps;
    ar["spins"] >> spins;
    ar.set_context(context);
}

void ising_sim::evaluate(results_type& results){
    results.insert("Cv", (results["E2"]-results["E"]*results["E"])*beta*beta*nsite);
    results.insert("Binder", results["M4"]/(results["M2"]*results["M2"]));
}

